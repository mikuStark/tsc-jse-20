package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandArg();

    Collection<String> getCommandName();

    void create(AbstractCommand command);

}
